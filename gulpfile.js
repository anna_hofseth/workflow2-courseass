const gulp = require('gulp');
const watchLess = require('gulp-watch-less');
const cleanCSS = require('gulp-clean-css');
const less = require('gulp-less');
const browserSync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');

gulp.task('less', function(){
	gulp.src('less/*.less')
		.pipe(watchLess('less/*.less'))
		.pipe(less())
		.pipe(cleanCSS())
		.pipe(gulp.dest('./website/css'));
});

gulp.task('browser-sync', function(){
	browserSync.init(['**/*'], {
		server: {
			baseDir: './website/'
		}
	});
});

gulp.task('minify-images', function(){
	gulp.src('images-original/*.jpg')
	.pipe(imagemin())
	.pipe(gulp.dest('website/images/photos'))
});

gulp.task('default', ['less', 'browser-sync', 'minify-images'], function(){
	gulp.watch('less/*.less', ['less']);
});